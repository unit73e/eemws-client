# IEC 62325-504 client #

[![Build Status](https://drone.io/bitbucket.org/smree/eemws-client/status.png)](https://drone.io/bitbucket.org/smree/eemws-client/latest)

### What is this repository for? ###

This is a client implementation of IEC 62325-504 technical specification.

* **eemws-client** includes client classes to invoke the eem web services
* **eemws-kit** includes command line utilities to invoke the eem web services, as well as several GUI applications (browser, editor, ...).
* Version **1.1.0**

Please use `./gradlew install` and java 7.x in order to compile.

You will also need the artifacts at [eemws-core](https://bitbucket.org/smree/eemws-core)

Please refer to [eemws-core downloads](https://bitbucket.org/smree/eemws-core/downloads) for news and documentation.

### Who do I talk to? ###

* If you have questions please contact soportesios@ree.es
